import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Constants } from "src/app/services/constants";
import { AppService } from "src/app/services/app.service";
import { LocalCacheService } from "src/app/services/local-cache.service";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.scss"]
})
export class AddUserComponent implements OnInit {
  addUserForm: FormGroup;
  users = [];
  p: number = 1;
  constructor(
    private appService: AppService,
    private spinnerService: Ng4LoadingSpinnerService,
    private LocalCache: LocalCacheService
  ) {
    this.create_form();
  }

  ngOnInit() {
    this.getAllUsers();
  }

  create_form() {
    this.addUserForm = new FormGroup({
      username: new FormControl("", [Validators.required]),
      firstname: new FormControl("", [Validators.required]),
      lastname: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required])
    });
  }

  getAllUsers() {
    this.appService.fetchAllUsers().then((resp: any) => {
      this.users = resp.data;
    });
  }

  addUser() {
    this.spinnerService.show();
    this.appService.addUser(this.addUserForm.value).then((resp: any) => {
      this.appService.showSimpleAlert(
        "User",
        resp.message,
        resp.success ? "success" : "error"
      );
      if (resp) {
        this.addUserForm.reset();
        this.getAllUsers();
        this.spinnerService.hide();
      }
    });
  }
}
