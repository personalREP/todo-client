import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { UserRoutingModule } from "./user-routing.module";
import { AddUserComponent } from "./add-user/add-user.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { SharedModule } from "src/app/shared/shared.module";
import { NgxPaginationModule } from "ngx-pagination";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";
import { NgxPasswordToggleModule } from "ngx-password-toggle";

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    NgxPaginationModule,
    Ng4LoadingSpinnerModule.forRoot(),
    NgxPasswordToggleModule
  ],
  declarations: [AddUserComponent]
})
export class UserModule {}
