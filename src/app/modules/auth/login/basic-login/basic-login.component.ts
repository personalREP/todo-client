import { Component, OnInit } from "@angular/core";
import { AppService } from "src/app/services/app.service";
import { LocalCacheService } from "src/app/services/local-cache.service";
import { Constants } from "src/app/services/constants";

@Component({
  selector: "app-basic-login",
  templateUrl: "./basic-login.component.html",
  styleUrls: ["./basic-login.component.scss"]
})
export class BasicLoginComponent implements OnInit {
  user: any = {
    username: "",
    password: ""
  };

  remember: boolean = false;

  constructor(
    private appService: AppService,
    private localCache: LocalCacheService
  ) {}

  ngOnInit() {}

  loginUser() {
    this.appService.userLogin(this.user).then((resp: any) => {
      if (resp.success) {
        this.localCache.setLoginData(resp.data);
        this.appService.navigateToView(Constants.VIEW_ROUTES.TODO);
      } else {
        this.appService.showSimpleAlert(
          "Authentication",
          resp.message,
          "error"
        );
      }
    });
  }
}
