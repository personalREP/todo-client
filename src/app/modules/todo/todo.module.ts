import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { TodoRoutingModule } from "./todo-routing.module";
import { AddTodoComponent } from "./add-todo/add-todo.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { SharedModule } from "src/app/shared/shared.module";
import { NgxPaginationModule } from "ngx-pagination";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";
import { NgxPasswordToggleModule } from "ngx-password-toggle";

@NgModule({
  imports: [
    CommonModule,
    TodoRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    NgxPaginationModule,
    Ng4LoadingSpinnerModule.forRoot(),
    NgxPasswordToggleModule
  ],
  declarations: [AddTodoComponent]
})
export class TodoModule {}
