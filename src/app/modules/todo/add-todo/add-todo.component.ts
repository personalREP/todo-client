import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Constants } from "src/app/services/constants";
import { AppService } from "src/app/services/app.service";
import { LocalCacheService } from "src/app/services/local-cache.service";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";

@Component({
  selector: "app-add-todo",
  templateUrl: "./add-todo.component.html",
  styleUrls: ["./add-todo.component.scss"]
})
export class AddTodoComponent implements OnInit {
  @ViewChild("inputForm") inputEl: ElementRef;
  addTodoForm: FormGroup;
  isUpdate: Boolean = false;
  todos = [];
  p: number = 1;
  constructor(
    private appService: AppService,
    private spinnerService: Ng4LoadingSpinnerService,
    private LocalCache: LocalCacheService
  ) {
    this.create_form();
  }

  ngOnInit() {
    this.getAllTodos();
  }

  create_form() {
    this.addTodoForm = new FormGroup({
      _id: new FormControl(""),
      title: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
      status: new FormControl("pending", [Validators.required])
    });
  }

  getAllTodos() {
    this.appService.fetchAllTodos().then((resp: any) => {
      this.todos = resp.data;
    });
  }

  addTodo() {
    this.spinnerService.show();
    if (!this.isUpdate) {
      this.appService.addTodo(this.addTodoForm.value).then((resp: any) => {
        this.appService.showSimpleAlert(
          "Todo",
          resp.message,
          resp.success ? "success" : "error"
        );
        if (resp) {
          this.addTodoForm.reset();
          this.getAllTodos();
          this.spinnerService.hide();
        }
      });
    } else {
      this.appService.updateTodo(this.addTodoForm.value).then((resp: any) => {
        this.appService.showSimpleAlert(
          "Todo",
          resp.message,
          resp.success ? "success" : "error"
        );
        if (resp) {
          this.isUpdate = false;
          this.addTodoForm.reset();
          this.getAllTodos();
          this.spinnerService.hide();
        }
      });
    }
  }

  deleteTodo(todo) {
    this.appService
      .showConfirmationAlert(
        "Remove",
        "Are you sure you want to remove this?",
        "YES"
      )
      .then(flag => {
        if (flag) {
          this.appService.deleteTodo({ _id: todo._id }).then((resp: any) => {
            this.appService.showSimpleAlert(
              "Todo",
              resp.message,
              resp.success ? "success" : "error"
            );
            if (resp) {
              this.isUpdate = false;
              this.addTodoForm.reset();
              this.getAllTodos();
              this.spinnerService.hide();
            }
          });
        }
      });
  }

  updateTodo(data) {
    let context = this;
    setTimeout(() => context.inputEl.nativeElement.focus());
    this.isUpdate = true;
    this.addTodoForm.get("_id").setValue(data._id);
    this.addTodoForm.get("title").setValue(data.title);
    this.addTodoForm.get("description").setValue(data.description);
    this.addTodoForm.get("status").setValue(data.status);
  }
}
