import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AdminComponent } from './layout/admin/admin.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AuthComponent } from './layout/auth/auth.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { MenuItems } from './shared/menu-items/menu-items';
import { BreadcrumbsComponent } from './layout/admin/breadcrumbs/breadcrumbs.component';
import { CanActivateAuthLayoutGuard } from './services/guards/can-activate-auth-layout.gaurd';
import { CanActivateHomeLayoutGuard } from './services/guards/can-activate-home-layout.guard';
import { LocalCacheService } from './services/local-cache.service';
import { AppService } from './services/app.service';
import { ApiService } from './services/api.services';
import { HttpModule } from '@angular/http';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';


@NgModule({


  declarations: [
    AppComponent,
    AdminComponent,
    AuthComponent,
    BreadcrumbsComponent
  ],


  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    HttpModule,
    Ng4LoadingSpinnerModule.forRoot()
  ],

  providers: [MenuItems, CanActivateAuthLayoutGuard, CanActivateHomeLayoutGuard, LocalCacheService, AppService, ApiService, { provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
