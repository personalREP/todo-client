import { Injectable } from "@angular/core";

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  short_label?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: "Users",
    main: [
      {
        state: "user",
        short_label: "U",
        name: "Users",
        type: "link",
        icon: "ti-user"
      }
    ]
  },
  {
    label: "Todo",
    main: [
      {
        state: "todo",
        short_label: "U",
        name: "Todos",
        type: "link",
        icon: "ti-view-list"
      }
    ]
  }
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }
}
