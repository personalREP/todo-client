import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AdminComponent } from "./layout/admin/admin.component";
import { AuthComponent } from "./layout/auth/auth.component";
import { CanActivateAuthLayoutGuard } from "./services/guards/can-activate-auth-layout.gaurd";
import { CanActivateHomeLayoutGuard } from "./services/guards/can-activate-home-layout.guard";

const routes: Routes = [
  {
    path: "",
    component: AdminComponent,
    children: [
      {
        path: "",
        redirectTo: "todo",
        pathMatch: "full"
      },
      { path: "**", redirectTo: "todo/", pathMatch: "full" },
      {
        path: "user",
        loadChildren: "./modules/user/user.module#UserModule"
      },
      {
        path: "todo",
        loadChildren: "./modules/todo/todo.module#TodoModule"
      }
    ],
    canActivate: [CanActivateHomeLayoutGuard]
  },
  {
    path: "",
    component: AuthComponent,
    children: [
      {
        path: "auth",
        loadChildren: "./modules/auth/auth.module#AuthModule"
      }
    ],
    canActivate: [CanActivateAuthLayoutGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
