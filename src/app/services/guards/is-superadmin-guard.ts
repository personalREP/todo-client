import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AppService } from '../app.service';
import { Constants } from '../constants';
import { LocalCacheService } from '../local-cache.service';

@Injectable()
export class IsSuperAdminGuard implements CanActivate {
    role: any

    constructor(private localCache: LocalCacheService, private appService: AppService) {

    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        this.role = this.localCache.getRole();

        if (this.role == "super_admin") {
            return true;
        }
        else {
            return false;
        }
    }
}
