import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { Observable } from "rxjs/Observable";

import { AppService } from "../app.service";
import { Constants } from "../constants";
import { LocalCacheService } from "../local-cache.service";

@Injectable()
export class CanActivateAuthLayoutGuard implements CanActivate {
  constructor(
    private localCache: LocalCacheService,
    private appService: AppService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.localCache.isUserLoggedIn()) {
      return true;
    }
    this.appService.navigateToView(Constants.VIEW_ROUTES.TODO);
    return false;
  }
}
