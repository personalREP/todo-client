export class Constants {
  public static BASE_URL = "http://localhost:5000/api/";
  public static SERVER_URL = "http://localhost:5000/api/";

  public static REQUEST_TYPE = {
    GET: "GET",
    POST: "POST",
    PUT: "PUT",
    DELETE: "DELETE"
  };

  public static API_ROUTES = {
    USER_LOGIN: {
      route: "user/login",
      type: Constants.REQUEST_TYPE.POST
    },
    CHANGE_PASSWORD: {
      route: "user/password",
      type: Constants.REQUEST_TYPE.PUT
    },

    ADD_USER: {
      route: "user",
      type: Constants.REQUEST_TYPE.POST
    },
    FETCH_ALL_USER: {
      route: "user",
      type: Constants.REQUEST_TYPE.GET
    },
    USER_DETAILS: {
      route: "user/",
      type: Constants.REQUEST_TYPE.GET
    },
    UPDATE_USER: {
      route: "user",
      type: Constants.REQUEST_TYPE.PUT
    },

    ADD_TODO: {
      route: "task",
      type: Constants.REQUEST_TYPE.POST
    },
    FETCH_ALL_TODO: {
      route: "task",
      type: Constants.REQUEST_TYPE.GET
    },
    TODO_DETAILS: {
      route: "task/",
      type: Constants.REQUEST_TYPE.GET
    },
    UPDATE_TODO: {
      route: "task",
      type: Constants.REQUEST_TYPE.PUT
    },
    DELETE_TODO: {
      route: "task",
      type: Constants.REQUEST_TYPE.DELETE
    }
  };

  public static VIEW_ROUTES = {
    LOGIN: "auth/login",
    USER: "/user",
    TODO: "/todo"
  };

  public static STORAGE_ITEM = {
    LOGIN_DATA: "_LUD",
    TOKEN: "_UT",
    ROLE: "role"
  };
}
