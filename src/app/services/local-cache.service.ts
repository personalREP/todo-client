import { Injectable } from '@angular/core';
import { Constants } from './constants';

@Injectable()
export class LocalCacheService {

  constructor() { }

  private setItem(key, value) {
    window.localStorage.setItem(key, value);
  }

  private getItem(key) {
    return window.localStorage.getItem(key);
  }

  private clearItem(key) {
    window.localStorage.removeItem(key);
  }

  public clearTokenErrMsg() {
    this.clearItem('ex_token_msg');
  }
  public setLoginData(data) {
    // this.setItem(Constants.STORAGE_ITEM.TOKEN, data.user.token);
    // this.setItem(Constants.STORAGE_ITEM.LOGIN_DATA, JSON.stringify(data.user));
    this.setItem(Constants.STORAGE_ITEM.TOKEN, data.token);
    this.setItem(Constants.STORAGE_ITEM.LOGIN_DATA, JSON.stringify(data));

  }

  // setGmtTimeZone(timeZone) {
  //   this.setItem(Constants.STORAGE_ITEM.GMT, timeZone);
  // }
  // public getGmtTimeZone() {
  //   return this.getItem(Constants.STORAGE_ITEM.GMT);
  // }

  public setEditId(key, value) {
    this.setItem(key, value);
  }
  public getEditId(key) {
    return this.getItem(key);
  }
  public ClearEditId(key) {
    this.clearItem(key);
  }

  public setGateway(gateway) {
    this.setItem('GATEWAY', gateway);
  }
  setTokeExpireErrorMsg(msg) {
    localStorage.setItem('ex_token_msg', msg);
  }
  getTokenExpireErrorMsg() {
    localStorage.getItem('ex_token_msg')
    // return this.tokenEexiredErrMsg;
  }
  public getGateway() {
    return this.getItem('GATEWAY');
  }
  public getLoginData() {
    return JSON.parse(this.getItem(Constants.STORAGE_ITEM.LOGIN_DATA));
  }

  public getToken() {
    return this.getItem(Constants.STORAGE_ITEM.TOKEN);
  }

  public getCurrentUser() {
    return this.getLoginData();
  }

  public isUserLoggedIn() {
    return this.getToken() != null;
  }

  public logoutUser() {
    this.clearItem(Constants.STORAGE_ITEM.LOGIN_DATA);
    this.clearItem(Constants.STORAGE_ITEM.TOKEN);
    // this.clearItem(Constants.STORAGE_ITEM.GMT);
  }

  public getRole() {
    return this.getItem(Constants.STORAGE_ITEM.ROLE);
  }

}
