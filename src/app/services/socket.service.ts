import { Injectable } from '@angular/core';
import { Socket } from 'ng-socket-io';

@Injectable()
export class SocketService {

  constructor(private socket: Socket) { }

  sendMessage(event: string, msg: any) {
    try {
      this.socket.emit(event, msg);
    } catch (ex) {
      console.log(ex);
    }

  }

  getMessage(event) {
    return this.socket
      .fromEvent(event);
  }

  close() {
    this.socket.removeAllListeners();
  }

}
