import { Injectable } from "@angular/core";
import { Constants } from "./constants";
import { ApiService } from "./api.services";
import { Router } from "@angular/router";
import "rxjs/add/operator/map";
import swal from "sweetalert2";

@Injectable()
export class AppService {
  items: any;

  constructor(private apiService: ApiService, private router: Router) {}

  setItems(set_items) {
    this.items = set_items;
  }
  getItems() {
    return this.items;
  }
  public navigateToView(path, id = null) {
    if (id != null) {
      this.router.navigate([path, id]);
    } else {
      this.router.navigate([path]);
    }
  }
  public showConfirmationAlert(title, text, confirmButtonText) {
    return new Promise((resolve, reejct) => {
      swal({
        title,
        text,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText
      }).then(result => {
        if (result.value) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  }

  public showSimpleAlert(title, text, type) {
    swal({
      title,
      text,
      type
    }).catch(swal.noop);
  }

  // Authentication
  public userLogin(body) {
    return new Promise((resolve, reject) => {
      this.apiService
        .generateApiRequest({
          route: Constants.API_ROUTES.USER_LOGIN.route,
          type: Constants.API_ROUTES.USER_LOGIN.type,
          body: body
        })
        .map(res => res.json())
        .subscribe(
          response => {
            resolve(response);
          },
          err => {
            resolve(err.json());
          }
        );
    });
  }

  public addUser(todo) {
    return new Promise((resolve, reject) => {
      this.apiService
        .generateApiRequest(
          {
            route: Constants.API_ROUTES.ADD_USER.route,
            type: Constants.API_ROUTES.ADD_USER.type,
            body: todo
          },
          true
        )
        .map(res => res.json())
        .subscribe(response => {
          resolve(response);
        });
    });
  }

  public fetchAllUsers() {
    return new Promise((resolve, reject) => {
      this.apiService
        .generateApiRequest(
          {
            route: Constants.API_ROUTES.FETCH_ALL_USER.route,
            type: Constants.API_ROUTES.FETCH_ALL_USER.type
          },
          true
        ) // },true)
        .map(res => res.json())
        .subscribe(response => {
          resolve(response);
        });
    });
  }

  public addTodo(todo) {
    return new Promise((resolve, reject) => {
      this.apiService
        .generateApiRequest(
          {
            route: Constants.API_ROUTES.ADD_TODO.route,
            type: Constants.API_ROUTES.ADD_TODO.type,
            body: todo
          },
          true
        )
        .map(res => res.json())
        .subscribe(response => {
          resolve(response);
        });
    });
  }

  public updateTodo(todo) {
    return new Promise((resolve, reject) => {
      this.apiService
        .generateApiRequest(
          {
            route: Constants.API_ROUTES.UPDATE_TODO.route,
            type: Constants.API_ROUTES.UPDATE_TODO.type,
            body: todo
          },
          true
        )
        .map(res => res.json())
        .subscribe(response => {
          resolve(response);
        });
    });
  }

  public deleteTodo(todo) {
    return new Promise((resolve, reject) => {
      this.apiService
        .generateApiRequest(
          {
            route: Constants.API_ROUTES.DELETE_TODO.route,
            type: Constants.API_ROUTES.DELETE_TODO.type,
            body: todo
          },
          true
        )
        .map(res => res.json())
        .subscribe(response => {
          resolve(response);
        });
    });
  }

  public fetchAllTodos() {
    return new Promise((resolve, reject) => {
      this.apiService
        .generateApiRequest(
          {
            route: Constants.API_ROUTES.FETCH_ALL_TODO.route,
            type: Constants.API_ROUTES.FETCH_ALL_TODO.type
          },
          true
        ) // },true)
        .map(res => res.json())
        .subscribe(response => {
          resolve(response);
        });
    });
  }

  public groupBy(data, key) {
    var result = data.reduce(function(r, a) {
      r[a[key]] = r[a[key]] || [];
      r[a[key]].push(a);
      return r;
    }, Object.create(null));
    return result;
  }
  public relativeTimeDifferenceFromNow(current, previous) {
    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;

    var elapsed = current - previous;

    if (elapsed < msPerMinute) {
      return Math.round(elapsed / 1000) + " seconds ago";
    } else if (elapsed < msPerHour) {
      return Math.round(elapsed / msPerMinute) + " minutes ago";
    } else if (elapsed < msPerDay) {
      return Math.round(elapsed / msPerHour) + " hours ago";
    } else if (elapsed < msPerMonth) {
      return Math.round(elapsed / msPerDay) + " days ago";
    } else if (elapsed < msPerYear) {
      return Math.round(elapsed / msPerMonth) + " months ago";
    } else {
      return Math.round(elapsed / msPerYear) + " years ago";
    }
  }

  public getDayEndTime(timestamp) {
    var date = new Date(timestamp);
    date.setHours(23, 59, 59, 59);
    return date;
  }

  public getDayStartTime(timestamp) {
    var date = new Date(timestamp);
    date.setHours(0, 0, 0, 0);
    return date;
  }

  public getFormattedDate(timestamp) {
    var date = new Date(timestamp);
    var dateString =
      date.getMonth() +
      1 +
      "/" +
      date.getDate() +
      " " +
      date.getHours() +
      ":" +
      date.getMinutes() +
      ":" +
      date.getSeconds();
    return dateString;
  }

  public getTimeForGMT(gmt) {
    var d = new Date();
    var utc = d.getTime() + d.getTimezoneOffset() * 60000;
    var nd = new Date(utc + 3600000 * gmt).toString();
    nd = nd.split("G")[0];
    return nd;
  }

  public ChangeTimeForGMT(gmt, givenDate) {
    var d = new Date(givenDate);
    var utc = d.getTime() + d.getTimezoneOffset() * 60000;
    var nd = new Date(utc + 3600000 * gmt).toString();
    nd = nd.split("G")[0];
    return nd;
  }

  public getRandomColor() {
    var letters = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
}
