# Todo Admin Panel!

Web Client for ToDo Challenge

# Installation

Open the directory where you cloned this. Then run `npm install` to install dependencies.

# Configuration

To configure the application `constants.ts` is the file where you can manage the API Endpoints etc.

# Run

To run the API server you just need to run this command in terminal `npm run start`. This will run the Angular Application on development mode.
Currently API Endpoint is for your local machine. So first run the API server and then run this Web client.
